const Koa = require('koa')
const serve = require('koa-static')
const SocketIO = require('socket.io')
const SocketIOStream = require('socket.io-stream')
const http = require('http')
const googleCloudLanguage = require('@google-cloud/language')
const googleCloudSpeech = require('@google-cloud/speech')
const fs = require('fs')
const stream = require('stream')
const path = require('path')
const config = require('./config')

const apiCredentials = {
  projectId: 'brian-presentation',
  keyFilename: './key.json'
}

const app = new Koa()
const port = 4000

app.use(serve('./public'))

const server = http.createServer(app.callback())
const io = new SocketIO(server)

const samplesDir = 'samples'

if (!fs.existsSync(samplesDir)) {
  fs.mkdirSync(samplesDir)
}

const wavFileName = 'test.wav'
const wavFilePath = path.join(samplesDir, wavFileName)

const encoding = 'LINEAR16'
const sampleRateHertz = 44100
const languageCode = 'en-US'
const speechRequest = {
  config: { encoding, sampleRateHertz, languageCode },
  interimResults: false
}
const languageClient = googleCloudLanguage(apiCredentials)
const speechClient = googleCloudSpeech(apiCredentials)

class ReconnectStream extends stream.Writable {
  constructor(options) {
    super(options)

    options.delay = options.delay ? options.delay : 0
    const errorCallback = () => {
      console.log('ReconnectStream recreating stream')
      setTimeout(() => this._stream = options.streamConstructor().on('error', errorCallback), options.delay)
    }
    this._stream = options.streamConstructor().on('error', errorCallback)
  }

  _write(chunk, encoding, next) {
    this._stream.write(chunk)
    next()
  }
}

io.on('connection', socket => {
  const recognizeStreamConstructor = () => speechClient.createRecognizeStream(speechRequest)
    .on('error', error => console.error('recognizeStream error:', error))
    .on('data', data => {
      console.log('SocketIO recognizeStream data:', data)

      if (data.error) {
        return
      }

      const sentence = data.results

      Promise.all([
        languageClient.detectSentiment(sentence),
        languageClient.detectEntities(sentence)
      ]).then(results => {
        socket.emit('recognitionResult', {
          sentence,
          sentiment: results[0][0],
          entities: results[1][0]
        })
      }).catch(error => console.error('SocketIO languageClient error:', error))
    })

  const reconnectRecognizeStream = new ReconnectStream({ streamConstructor: recognizeStreamConstructor, delay: 300 })
    .on('error', error => console.error('reconnectRecognizeStream error:', error))
    .on('data', data => console.log('reconnectRecognizeStream data:', data))

  SocketIOStream(socket).on('voiceStream', voiceStream => {
    console.log('SocketIOStream voiceStream')

    voiceStream.on('end', () => console.log('SocketIOStream voiceStream end'))
    voiceStream.on('error', (err) => console.error('SocketIOStream voiceStream error:', err))

    voiceStream.pipe(reconnectRecognizeStream)

    if (config.enableWavRecording) {
      const wavHeaderStream = fs.createReadStream('wavHeader.bin')
      wavHeaderStream.on('end', () => voiceStream.pipe(fs.createWriteStream(wavFilePath, { flags: 'a' })))
      wavHeaderStream.pipe(fs.createWriteStream(wavFilePath))
    }
  })

  socket.on('disconnect', () => {
    console.log('socket.io disconnect')
  })
})

server.listen(port)