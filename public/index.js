const imageFolder = 'images'
const imageNames = [
  'start.jpg',
  'pipes.jpg',
  'electricity-cable.jpg',
  'food-supplies.jpg',
  'workers.jpg',
  'finish.jpg'
]
const descriptions = [
  'One day Mr. Jones went to a factory to make an inspection. Say "Next" to proceed or click the next button.',
  'He then found some troubles with pipes. (Say something about pipes.)',
  'An electricity cable was not in the best condition too. (Say something about electricity cable.)',
  'The food supplies were infested by rats. (Say something about food supplies.)',
  'Although workers there were very happy. (Say something about workers.)',
  'The moment he came home his report had already been waiting for him on his computer.',
]
const relatedWordsByScreen = {
  1: ['pipe', 'pipes', 'tube', 'tubes', 'water', 'leak'],
  2: ['cable', 'cables', 'cord', 'cords', 'electricity', 'power'],
  3: ['food', 'supply', 'supplies', 'rat', 'rats', 'floor', 'dirtiness'],
  4: ['work', 'worker', 'workers', 'happiness', 'employee', 'employees']
}
const speechBubblePositions = [
  { top: 635, left: 585 },
  { top: 632, left: 473 },
  { top: 554, left: 678 },
  { top: 593, left: 614 },
  { top: 624, left: 579 },
  { top: 500, left: 700 }
]
const screensAmount = 6
const microphoneThreshold = 0.1
const preloader = new Image()
let currentScreenIndex = 0
let isMicrophoneListening = false
let textTimer

function preloadScreen(screenIndex) {
  preloader.src = `${imageFolder}/${imageNames[screenIndex]}`
}

function isLastScreen(screenIndex) {
  return screenIndex === screensAmount - 1
}

function switchScreen(screenIndex) {
  for (let i = 0; i < screensAmount; i++) {
    unhighlightNavigatorCircle(i)
  }

  highlightNavigatorCircle(screenIndex)

  document.getElementById('description-text').innerHTML = descriptions[screenIndex]
  document.getElementById('picture').style.backgroundImage = `url(${imageFolder}/${imageNames[screenIndex]})`

  repositionSpeechBubble(screenIndex)

  if (isLastScreen(screenIndex)) {
    document.getElementById('next-button').classList.add('hidden')
  }
  else {
    document.getElementById('next-button').classList.remove('hidden')
    preloadScreen(screenIndex + 1)
  }

  currentScreenIndex = screenIndex
}

function highlightNavigatorCircle(circleIndex) {
  document.getElementById(`navigator-circle-${circleIndex}`).classList.add('navigator-circle-highlight')
}

function unhighlightNavigatorCircle(circleIndex) {
  document.getElementById(`navigator-circle-${circleIndex}`).classList.remove('navigator-circle-highlight')
}

function nextScreen() {
  switchScreen(currentScreenIndex + 1)
}

function tickCheckbox(screenIndex) {
  document.getElementById(`checkbox-tick-${screenIndex}`).classList.remove('hidden')
  document.getElementById(`checkbox-cross-${screenIndex}`).classList.add('hidden')
}

function crossCheckbox(screenIndex) {
  document.getElementById(`checkbox-tick-${screenIndex}`).classList.add('hidden')
  document.getElementById(`checkbox-cross-${screenIndex}`).classList.remove('hidden')
}

function isChecklistScreen(screenIndex) {
  return 1 <= screenIndex && screenIndex <= 4
}

function repositionSpeechBubble(screenIndex) {
  const speechBubblePosition = speechBubblePositions[screenIndex]
  const screenWidth = window.innerWidth
  const screenHeight = window.innerHeight
  const pictureWidth = 1920
  const pictureHeight = 1080
  const originalVectorX = speechBubblePosition.left - pictureWidth / 2
  const originalVectorY = speechBubblePosition.top - pictureHeight / 2
  let vectorX, vectorY

  if (screenWidth / screenHeight < pictureWidth / pictureHeight) {
    // vector = f(screenHeight)
    const scale =  screenHeight / pictureHeight
    vectorX = scale * originalVectorX
    vectorY = scale * originalVectorY
  }
  else {
    // vector = f(screenWidth)
    const scale =  screenWidth / pictureWidth
    vectorX = scale * originalVectorX
    vectorY = scale * originalVectorY
  }

  document.getElementById('speech-bubble-container').style.marginLeft = vectorX
  document.getElementById('speech-bubble-container').style.marginTop = vectorY
}

function showMicrophoneFeedback() {
  document.getElementById('speech-bubble-microphone').classList.add('active-microphone')
}

function hideMicrophoneFeedback() {
  document.getElementById('speech-bubble-microphone').classList.remove('active-microphone')
}

window.addEventListener('resize', () => {
  repositionSpeechBubble(currentScreenIndex)
}, false)

window.addEventListener('load', () => {
  switchScreen(currentScreenIndex)
}, false)

const Buffer = buffer.Buffer
const audioContext = new AudioContext()
const constraints = {
  audio: true,
  video: false
}
const bufferSize = 2048
const socket = io()
const voiceStream = ss.createStream()

function float32ArrayToBuffer(float32Array) {
  const resultArrayBuffer = new ArrayBuffer(float32Array.length * 2)
  const resultDataView = new DataView(resultArrayBuffer)

  for (let i = 0, offset = 0; i < float32Array.length; i++, offset += 2) {
    const s = Math.max(-1, Math.min(1, float32Array[i]))
    resultDataView.setInt16(offset, s < 0 ? s * 0x8000 : s * 0x7FFF, true)
  }

  return Buffer.from(resultArrayBuffer)
}

socket.on('connect', () => {
  console.log('socket.io connect')

  navigator.mediaDevices.getUserMedia(constraints).then(stream => {
    const sourceNode = audioContext.createMediaStreamSource(stream)
    const scriptNode = audioContext.createScriptProcessor(bufferSize, 1, 1)
    let micFeedbackTimer

    scriptNode.onaudioprocess = audioProcessingEvent => {
      const audioBuffer = audioProcessingEvent.inputBuffer
      const float32Array = audioBuffer.getChannelData(0)
      const outputBuffer = float32ArrayToBuffer(float32Array)
      let maxSample = 0

      for (let i = 0; i < float32Array.length; i++) {
        const sample = float32Array[i]
        const absSample = Math.abs(sample)
        if (absSample > maxSample) {
          maxSample = absSample
        }
      }

      if (maxSample > microphoneThreshold) {
        showMicrophoneFeedback()
        clearTimeout(micFeedbackTimer)
        micFeedbackTimer = setTimeout(hideMicrophoneFeedback, 300)
      }

      isMicrophoneListening = maxSample > microphoneThreshold
      voiceStream.write(outputBuffer)
    }

    sourceNode.connect(scriptNode)
    scriptNode.connect(audioContext.destination)
  }).catch(console.error)
})

ss(socket).emit('voiceStream', voiceStream)

socket.on('recognitionResult', data => {
  const { sentence, sentiment, entities } = data

  console.log('------------------------')
  console.log('socket.io sentence:', sentence)
  console.log('socket.io sentiment:', sentiment)
  console.log('socket.io entities', entities)
  console.log('socket.io entities names:', entities.map(entity => entity.name).join(', '))

  const hits = {}
  const screenKeys = Object.keys(relatedWordsByScreen)
  screenKeys.map(key => hits[key] = 0)

  entities.forEach(entity => {
    entity.name.split(' ').forEach(entityWord => {
      screenKeys.forEach(screenKey => {
        if (relatedWordsByScreen[screenKey].indexOf(entityWord.toLowerCase()) > -1) {
          hits[screenKey]++
        }
      })
    })
  })

  const sortedHitsEntries = Object.entries(hits).sort((a, b) => a[1] < b[1])
  const relatedScreenIndex = sortedHitsEntries[0][1] === sortedHitsEntries[1][1] ? -1 : sortedHitsEntries[0][0]

  console.log('relatedScreenIndex:', relatedScreenIndex)

  if (textTimer) {
    clearTimeout(textTimer)
  }
  document.getElementById('speech-bubble-text').innerHTML = sentence
  document.getElementById('speech-bubble').classList.add('with-text')
  textTimer = setTimeout(() => {
    document.getElementById('speech-bubble').classList.remove('with-text')
    document.getElementById('speech-bubble-text').innerHTML = ''
  }, 3000)

  if (isChecklistScreen(relatedScreenIndex)) {
    if (sentiment.score >= 0) {
      tickCheckbox(relatedScreenIndex)
    }
    else {
      crossCheckbox(relatedScreenIndex)
    }
  }

  if (!isLastScreen(currentScreenIndex) && relatedScreenIndex === -1) {
    if (sentence.split(' ').map(word => word.toLowerCase()).indexOf('next') !== -1) {
      nextScreen()
    }
  }
})

socket.on('disconnect', () => {
  console.log('socket.io disconnect')
})
