const fs = require('fs')

const configPath = './config.json'
const defaultConfig = {
  enableWavRecording: false
}

if (!fs.existsSync(configPath)) {
  fs.writeFileSync(configPath, JSON.stringify(defaultConfig, null, 2))
}

module.exports = require(configPath)